import os
import shutil
from types import *
from consolemenu import *
from consolemenu.items import *
from MyMultiSelectMenu import MyMultiSelectMenu
from consolemenu.format import *
from consolemenu.menu_component import Dimension
from pprint import pprint
from consolemenu import Screen
import json

CONFIG = False

def replacement(self):
    print "clear"

Screen.clear = replacement

class Config : 
    
    def __init__( self, filename=".devilconfig" ):

        self.filename = filename
        if os.path.isfile( filename ):
            print( "Loading " + filename )
            fd = open( self.filename,'r' )
            self.config = json.load( fd )
            fd.close()
        else:
            print( "Creating empty configuration" )
            self.config = self.emptyConfig()
            self.write()


    @staticmethod
    def getInstance( filename = None):
        global CONFIG
        if CONFIG:
            return CONFIG
        CONFIG = Config( filename )
        return CONFIG

    def write(self):
        if os.path.isfile( self.filename ):
            shutil.copy( self.filename, self.filename + "_backup" )
        fd = open( self.filename,'w' )
        json.dump( self.config, fd )
        fd.close()

    def emptyConfig( self ):
        return { 'sources'           : [],
                 'constraints'       : [],
                 'projectIPs'        : [],
                 'projectName'       : "my_project",
                 'projectDir'        : "./",
                 'build_dir'         : "../build_dir",
                 'part'              : "xcku15p-ffva1760-2-e",
                 'board'             : "xilinx.com:vcu118:part0:2.0",
                 'gitRepoPath'       : "../gitreps",
                 'packageIP_dir'     : "../build_IP",
                 'packageIP_configs' : []
             }

    def emptyPackageIPConfig( self ):
        return { 'name'             : '',
                 'sources'          : [],
                 'constraints'      : [],
                 'projectIPs'       : [],
                 'supportedFamilies': ["virtexuplus Production", "virtexu Production", "kintexu Production", "kintexuplus Production"],
                 'vendor'           : "CERN_CMS_CMD",
                 'url'              : "www.cern.ch",
                 'version'          : "1.0",
                 'part'             : "",
                 'categories'       : ["CERN_CMS_DAQ"]
        }


    def newPackageIPConfig( self, name, part ):
        nconfig = self.emptyPackageIPConfig()
        nconfig['part'] = part
        nconfig['name'] = name
        self.config['packageIP_configs'].append( nconfig )
        self.write()
        return nconfig


def removeFromList( current ):
    if len(current) == 0:
        print "Nothing to remove here (empty lists)"
        raw_input()
        return
    
    while True:
        ixarr = MyMultiSelectMenu.get_selection( current )
        
        if ixarr[-1] >= len(current):
            return
        for ix in reversed(ixarr):
            del current[ix]
        CONFIG.write()
        if len(current)==0:
            return

def findSources( dirlist, exto, excludelist=[]  ):
    print "find sources ", dirlist, exto, excludelist
    result = []
    if type( exto ) == ListType:
        extl = exto
    else:
        extl = [ exto ]
    print "extl", extl
    for ext in extl:
        for path in dirlist:
            tmp = findFiles( path, ext, excludelist )
            for it in tmp:
                result.append( it['path'] )
    
    return result

def addNotPresent( present, extensions ):
    
    Config = CONFIG
    config = Config.config
    gitreps = map( lambda x : os.path.join( config['gitRepoPath'], x ), os.listdir( config['gitRepoPath'] ) )
    ix = SelectionMenu.get_selection( gitreps, "Select one git repository" )
    if ix >= len( gitreps ):
        return
    choices = findSources([gitreps[ix]], extensions, present)


    if len(choices)==0:
        print "Already everything added here (or not source files present)"
        raw_input()
        return
        	
    while True:
        
        ixarr = MyMultiSelectMenu.get_selection( choices, "Add sources which are not already included", "You can select multiple files with a format like '1,2,4,6-12'")
        
        if ixarr[-1] >= len(choices):
            return
        for ix in ixarr:
            present.append( choices[ ix ] )
        Config.write()
        for ix in reversed(ixarr):
            del choices[ix]
        if len(choices)==0:
            return



class IPMaker :
    def __init__( self, name ):
        self.name = name
        self.getIPConfig()
        return self


    def getIPConfig(self):
        emptyConfig = { 'name' : self.name,
                        'output_dir': '../output_IP_' + self.name,
                        'sources' : [],
                        'constraints' : [],
                        'ips' : [],
                        'sourcepaths' : [],
                        'ippaths' : [],
                        'constraintPaths' : [],
                        'vendor' : "CERN",
                        'categories' : ['USER','CERN/CMS/CMD'],
                        'url' : "http://www.cern.ch" }
        
        # a new IP
        if not 'IP' in CONFIG:
            CONFIG["IP"] = { self.name : emptyConfig }
            self.config = CONFIG["IP"]
            return

        if not name in CONFIG["IP"]:
            CONFIG["IP"][name] = emptyConfig
            self.config = CONFIG["IP"]
            return

        self.config = CONFIG["IP"]
        for (k,v) in emptyConfig.iteritems():
            if not k in self.config:
                self.config[k] = v
        return

def yesNo( prompt, default ):
    val = ""
    while val != 'y' and val != 'n':
        val = raw_input( prompt + '(y/n) [' + default + '] : ' ) 
        if val == "":
            val = default
        val = val.lower()
        if val == 'n':
            return False
        if val == 'y':
            return True
        print( "Please answer 'y' or 'n'." )


def findFiles( topdir, searchext, excludelist = []  ):
    result = []
    for root, dirs, files in os.walk( topdir, topdown=True ):
        dirs[:] = [d for d in dirs if d not in [ '.git' ] ]
        for file in files:
            path = os.path.join( root, file )
            skip = False
            for pat in excludelist:
                if path.endswith( pat ):
                    skip = True
            if skip:
                continue
            name,ext = os.path.splitext( file )
            if ext == searchext:
                result.append( { 'path' : path,
                                 'name' : name,
                                 'file' : file } ) 
    return result





def sourceEditingMenuItems( sourcelist, constraintlist, iplist, appendMenu ):
    addSourcesItem = FunctionItem( "Add sources", addNotPresent, 
                                   kwargs = { 'present' : sourcelist,
                                              'extensions' : ['.vhd','.v'] }) 
    appendMenu.append_item( addSourcesItem )
    
    removeSourcesItem = FunctionItem( "Remove sources", removeFromList, 
                                   kwargs = { 'current' : sourcelist } )
    appendMenu.append_item( removeSourcesItem )
    
    
    
    addConstraintsItem = FunctionItem( "Add constraints", addNotPresent, 
                                       kwargs = { 'present' : constraintlist,
                                                  'extensions' : ['.xdc'] }) 
    appendMenu.append_item( addConstraintsItem )
    
    removeConstraintsItem = FunctionItem( "Remove constraints", removeFromList, 
                                          kwargs = { 'current' : constraintlist } )
    appendMenu.append_item( removeConstraintsItem )
    
    
    
    addProjectIPsItem = FunctionItem( "Add project IPs", addNotPresent, 
                                       kwargs = { 'present' : iplist,
                                                  'extensions' : ['.xci'] }) 
    appendMenu.append_item( addProjectIPsItem )
    
    removeProjectIPsItem = FunctionItem( "Remove project IPs", removeFromList, 
                                          kwargs = { 'current' : iplist } )
    appendMenu.append_item( removeProjectIPsItem )

def makeProjectScript():
    global CONFIG
    config = CONFIG.config

    projectName = config['projectName']
    projectDir = config['projectDir']

    build_dir = os.path.join( projectDir, projectName )
    ip_reprocess = False

    #if os.path.isdir( build_dir ):
    #    if not yesNo( "The build directory exists already: Do you want to keep it ? ", 'Y' ):
    #        print( "Removing existing output directory and starting from scratch..." )
    #        shutil.rmtree( build_dir, ignore_errors=True )
    #        os.makedirs( build_dir)
    #        ip_reprocess = True
    #else :
    #    os.makedirs( build_dir )
    
    
    tcl_script = "create_project " + projectName + " " + build_dir + " -part " + config['part'] + "\n"
    
    if config['board']:
        tcl_script += "\nset_property BOARD_PART " + config['board'] + " [current_project]\n"

    ######################################### IP treatment ############################################
    for ipsrc in config['projectIPs']:
        tcl_script += "import_files -norecurse " + ipsrc + "\n"
        #tcl_script += "export_ip_user_files -of_objects [get_files "
    
    for src in config['sources']:
        tcl_script += "import_files -norecurse " + src + "\n"

    for con in config['constraints']:
        tcl_script += "add_files -fileset constrs_1 -norecurse " + con + "\n"
        tcl_script += "import_files -fileset constrs_1 " + con + "\n"

    tcl_script += "\nupdate_compile_order -fileset sources_1\n"
    
    
    # output tcl script
    fd = open( projectName + "_create_project.tcl", "w")
    fd.write( tcl_script )
    fd.close() 


def makeBuildingScript():
    global CONFIG
    config = CONFIG.config

    build_dir = config['build_dir']
    ip_reprocess = False

    if os.path.isdir( build_dir ):
        if not yesNo( "The build directory exists already: Do you want to keep it ? ", 'Y' ):
            print( "Removing existing output directory and starting from scratch..." )
            shutil.rmtree( build_dir, ignore_errors=True )
            os.mkdir( build_dir)
            ip_reprocess = True
    else :
        os.mkdir( build_dir )
    
    iprunDir = os.path.join( build_dir, "ip_run" )
    if os.path.isdir( iprunDir ):
        if not yesNo( "The directory for compiled IPs exists already: Do you want to keep it ? ", 'Y' ):
            print( "Removing IP compilation directory and recompiling all IPs" )
            shutil.rmtree( iprunDir, ignore_errors=True )
            os.mkdir( iprunDir)
            ip_reprocess = True
    else:
        os.mkdir( iprunDir )
    
    tcl_script = ""    
    tcl_script += "set_part " + config['part'] + "\n"

    ######################################### IP treatment ############################################
    
    print "iprepro", ip_reprocess
    # copy the core definitions for building (and then upgrade and rebuild them)
    ip_recopy = False
    if not ip_reprocess:
        ip_recopy = not yesNo( "You want to copy the original IP sources before re-upgrade and re-build them? ", 'N' )
    else:
        ip_recopy = True
    
    print "ipreco", ip_recopy
    
    tcl_script += '\n# Read the IP definitions into memory\n'

    ipcores = []
    
    for ipsrc in config['projectIPs']:

        ipfile = os.path.basename(ipsrc)
        (ipname,ext) = os.path.splitext( ipfile )

        ip = { 'name' : ipname,
               'source' : ipsrc,
               'file' : ipfile }
        ipcores.append( ip )
        
        ipbuildDir = os.path.join( iprunDir, ipname )    
        if not os.path.isdir( ipbuildDir ):
            os.mkdir(ipbuildDir)
        if ip_recopy :
            shutil.copy( ipsrc, ipbuildDir )
        tcl_script += 'read_ip ' + os.path.join(ipbuildDir, ipfile) + "\n"
    
    if config['board']:
        tcl_script += "\nset_property BOARD_PART " + config['board'] + " [current_project]\n"
    
    tcl_script += '\n# Read the constraints\n'
    for ct in config['constraints']:
        tcl_script += 'read_xdc ' + ct + '\n'
    
    if ip_recopy:
        tcl_script += '\n# Upgrade the IPs\n'
        for ip in ipcores:
            tcl_script += 'upgrade_ip -vlnv [ get_ipdefs -of_objects [ get_ips ' + ip['name'] + ' ] ] [ get_ips ' + ip['name'] + ' ] -log ip_upgrade_' + ip['name'] + '.log\n' 
    
        tcl_script += '\n# Synthesize the IPs\n'
        for ip in ipcores:
            tcl_script += 'synth_ip [get_ips ' + ip['name'] + ']\n'
    
    
    ####################################### end of IP treatment #########################################

    tcl_script += '\n# Synthesize the design\n'
    for src in config['sources']:
        if src[-4:] == ".vhd":
            tcl_script += 'read_vhdl ' + src + '\n'
        elif src[-2:] == ".v":
            tcl_script += 'read_verilog ' +  src + '\n'
    
    ### some extras
    tcl_script += "\nremove_files top_Example.vhd \n"
    tcl_script += "\nupdate_compile_order \n"
    
    tcl_script += '\n# Now synthesize the entire design\n'
    tcl_script += 'synth_design -top top\n'
    tcl_script += 'opt_design \n'
    tcl_script += 'place_design \n'
    
    # output tcl script
    fd = open( "autosynth_devil.tcl", "w")
    fd.write( tcl_script )
    fd.close() 



def IPParameterChanger( ipPackProj ):

    params = []
    pdat = []
    for key in ipPackProj:
        if key == "sources":
            continue
        if key == "constraints":
            continue
        if key == "projectIPs":
            continue
        if key == "packageIP_configs":
            continue

        val = ipPackProj[key]
        it = "{0:20s} ==> {1:s}".format( key, val )
        params.append(it)
        pdat.append( [key,val] )

        

    if len(params) == 0:
        print "Nothing to change here (no parameters)"
        raw_input()
        return
    
    while True:
        ix = SelectionMenu.get_selection( params )
        
        if ix >= len(params):
            return

        newval = raw_input( pdat[ix][0] + "  [ " + repr(pdat[ix][1]) + " ]  : " )
            
        
        if newval == "" :
            return

        if isinstance(pdat[ix][1], list):
            newlist = []
            for it in newval.split(','):
                newlist.append(it)
            ipPackProj[ pdat[ix][0] ] = newlist
            pdat[ix][1] = newlist
            params[ix] = "{0:20s} ==> {1:s}".format( pdat[ix][0], pdat[ix][1] )
        else:
            ipPackProj[ pdat[ix][0] ] = newval
            pdat[ix][1] = newval
            params[ix] = "{0:20s} ==> {1:s}".format( pdat[ix][0], pdat[ix][1] )
 
        CONFIG.write()
    
    

def makeIPPackagingScript( ipPackProj ):
    global CONFIG
    name = ipPackProj['name']
    
    
    # start from scratch:
    packdir = os.path.join( CONFIG.config['packageIP_dir'], name )
    shutil.rmtree( packdir, ignore_errors=True )
    os.makedirs( packdir )
    tcl = "set version [lindex $argv 0]\n"
    tcl += "puts $version\n"
    tcl += "\n"
    tcl += "create_project " + name + " " + packdir + " -part " + ipPackProj['part'] + "\n"
    tcl += "\n"
    #tcl +="ipx::infer_core -vendor " + ipPackProj['vendor'] + " -library user -taxonomy {"
    #for cat in ipPackProj['categories']:
    #    tcl += "/" + cat + " "
    #tcl += "} " + name + "\n"
    #tcl += "ipx::edit_ip_in_project -upgrade true -name edit_" + name + " -directory " + os.path.join( packdir, name + "_tmp" ) + " " + os.path.join( packdir, "component.xml" ) + "\n"
    #tcl += "ipx::current_core " + os.path.join( packdir, "component.xml" ) + "\n"
    tcl += "import_files {" 
    for source in ipPackProj['sources']:
        tcl += source + " " 
    tcl += "}\n"
    tcl += "import_files {"
    for ip in ipPackProj['projectIPs']:
        tcl += ip + " " 
    tcl += "}\n"
    tcl += "import_files -fileset constrs_1 -norecurse {"
    for xdc in ipPackProj['constraints']:
        tcl += xdc + " " 
    tcl += "}\n\n"
    # ooc files must have porpoerties out of context and used in sythesis
    tcl+= "set_property USED_IN {synthesis implementation out_of_context} [ get_files -regexp .*_ooc.xdc ]\n"
   
    tcl += "update_compile_order -fileset sources_1\n\n"
    tcl += "upgrade_ip [get_ips ] -log ip_upgrade.log\n\n"

    # for every ip export simulation -of objects [get_files "filexci"]...


    tcl += "ipx::package_project -root_dir " + packdir + "/" + os.path.basename( packdir) + ".srcs -vendor " + ipPackProj['vendor'] + " -library user -taxonomy {"
    for cat in ipPackProj['categories']:
        tcl += "/" + cat + " "
    tcl += "} \n"
    tcl+= "set_property core_revision 2 [ipx::current_core]\n"
    tcl += "set_property supported_families {"
    for family in ipPackProj['supportedFamilies']:
        tcl += family + " "
    tcl += "} [ipx::current_core]\n"

    # Check if there is a customisation script to tweak the gui. If so execute it
    tcl += "if { [ file exists ipguibuilder_" + name + ".tcl ] } {\n"
    tcl += "    source ipguibuilder_" + name + ".tcl \n"
    tcl += "}\n"
    tcl+= "ipx::create_xgui_files [ipx::current_core]\n"
    tcl+= "ipx::update_checksums [ipx::current_core]\n"
    tcl+= "ipx::save_core [ipx::current_core]\n"

    # not sure if this ip_repo_path is useful for anything...
    tcl+= "set_property ip_repo_paths {" + packdir + "/" + os.path.basename( packdir) + ".srcs } [current_project]\n"
    tcl += "update_ip_catalog\n"
    tcl+= "ipx::check_integrity -quiet [ipx::current_core]\n"
    tcl+= "ipx::archive_core [join [list " + os.path.join(packdir, "cern.ch_user_" +  name + "_GLB_") + " $version .zip ] \"\" ] [ipx::current_core]\n"
 
    tclpath = os.path.join( "./", name + "_buildIp.tcl" )
    fd = open( tclpath, 'w' )
    fd.write( tcl )
    fd.close()

def testcolors():
    x = 0
    for i in range(24):
        colors = ""
        for j in range(5):
	    code = str(x+j)
	    colors = colors + "\33[" + code + "m\\33[" + code + "m\033[0m "
        print(colors)
        x=x+5

#CONFIG = Config()

# define some colors
CEND      = '\33[0m'
CBOLD     = '\33[1m'
CITALIC   = '\33[3m'
CURL      = '\33[4m'
CBLINK    = '\33[5m'
CBLINK2   = '\33[6m'
CSELECTED = '\33[7m'

CBLACK  = '\33[30m'
CRED    = '\33[31m'
CGREEN  = '\33[32m'
CYELLOW = '\33[33m'
CYELLOW2 = '\33[93m'
CBLUE   = '\33[34m'
CVIOLET = '\33[35m'
CBEIGE  = '\33[36m'
CWHITE  = '\33[37m'
CORANGEBG = '\33[38;5;208m'

CBLACKBG  = '\33[40m'
CREDBG    = '\33[41m'
CGREENBG  = '\33[42m'
CYELLOWBG = '\33[43m'
CYELLOW2BG = '\33[103m'
CBLUEBG   = '\33[44m'
CVIOLETBG = '\33[45m'
CBEIGEBG  = '\33[46m'
CWHITEBG  = '\33[47m'
CORANGEBG = '\33[48;5;208m'

CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CBEIGE2  = '\33[96m'
CWHITE2  = '\33[97m'

CGREYBG    = '\33[100m'
CREDBG2    = '\33[101m'
CGREENBG2  = '\33[102m'
CYELLOWBG2 = '\33[103m'
CBLUEBG2   = '\33[104m'
CVIOLETBG2 = '\33[105m'
CBEIGEBG2  = '\33[106m'
CWHITEBG2  = '\33[107m'
