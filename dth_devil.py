#!/usr/bin/python

import os
import sys
from pprint import pprint
from fpgatools import findFiles, Config, addNotPresent, removeFromList, sourceEditingMenuItems, makeIPPackagingScript, makeBuildingScript, makeProjectScript, IPParameterChanger, yesNo
from consolemenu import *
from consolemenu.format import *
from consolemenu.items import *
from MyMultiSelectMenu import MyMultiSelectMenu
from consolemenu.menu_component import Dimension
import argparse
from copy import deepcopy

basepath = os.path.expanduser("~/cmd_vivado_tools/devilConfigs")

parser = argparse.ArgumentParser( description="Generate Vivado scripts for project building and IP generation" )
parser.add_argument( '--list', help="Lists the possible configurations.", action="store_true")
parser.add_argument( 'configName', help="The name of the configuration.", nargs="?", default="")

args = parser.parse_args()

if args.list :
    
    for c in os.listdir( basepath ):
        if os.path.isfile( os.path.join(basepath, c) ):
            print(c)
    sys.exit()


cfile = os.path.expanduser(os.path.join( basepath, args.configName ))



print os.path.isfile(cfile)
if not os.path.isfile(cfile) :
    print( "\nThe configuration file " +  cfile + " does not exist!\n" ) 
    sys.exit()
    
print("Try to load ", cfile)

Config = Config.getInstance( cfile )
config = Config.config

gitreps = map( lambda x : os.path.join( config['gitRepoPath'], x ), os.listdir( config['gitRepoPath'] ) )


# Change some menu formatting
menu_format = MenuFormatBuilder( max_dimension = Dimension( width=160, height=50 ) ) \
    .set_border_style_type(MenuBorderStyleType.HEAVY_BORDER) \
    .set_prompt("SELECT>") \
    .set_title_align('center') \
    .set_subtitle_align('center') \
    .set_left_margin(4) \
    .set_right_margin(4) \
    .show_header_bottom_border(True)



menu = ConsoleMenu("DTH Devil", "Tools to facilitate the FPGA compilation", formatter=menu_format)


#######################################################################
# Configuration Editor
#######################################################################
submenu_configEdit = ConsoleMenu( "Configuration Editor", "Edit the configuration: Add/Delete sources, ips, constraints...",
                                  formatter = menu_format )
submenu_configEdit_item = SubmenuItem( "Configuration Editor", submenu = submenu_configEdit )
submenu_configEdit_item.set_menu( menu )
menu.append_item( submenu_configEdit_item )

def configDump():
    pprint (config)
    raw_input()
    return

configedit_item = FunctionItem("Edit the configuration", IPParameterChanger, args=[config] ) 
submenu_configEdit.append_item( configedit_item )
configdump_item = FunctionItem("Print the current configuration", configDump )
submenu_configEdit.append_item( configdump_item )


#######################################################################
# Create an build menu
#######################################################################
submenu_build = ConsoleMenu( "Build menu", prologue_text="Tools to build a design in non-project mode. Files are copied from the git repositories into a build area and tcl scripts are generated. Various options for the build process exist.",
                                formatter=menu_format )
submenu_build_item = SubmenuItem( "Build menu", submenu = submenu_build )
submenu_build_item.set_menu( menu )
menu.append_item( submenu_build_item )

sourceEditingMenuItems( config['sources'],config['constraints'],config['projectIPs'],submenu_build)

makebuildtcl = FunctionItem( "Generate tcl script for building the project", makeBuildingScript )
submenu_build.append_item( makebuildtcl )

makeprojecttcl = FunctionItem( "Generate tcl script for making a vivado project", makeProjectScript )
submenu_build.append_item( makeprojecttcl )


#######################################################################
# Create an IP packaging menu
#######################################################################
def newPackagingMenu():
    name = raw_input( "Enter name for new IP packaging project: ")
    if name == "" :
        return
    part = raw_input( "Enter the part for which the IP should be created" )
    if part == "" :
        return
    ipconfig = Config.newPackageIPConfig( name, part )
    packagingMenu( ipconfig )

# Copy an existing packaging menu to a new name
def copyPackagingMenu():

    # list existing IPs and let the user choose one
    ix = 1
    ch = []
    for ipconfig in config['packageIP_configs']:
        print( str(ix) + ") " + ipconfig['name'] )
        ix+=1
    num = int(raw_input( "Enter the configuration to copy: " ))

    name = raw_input( "Enter name for new IP packaging project: ")
    if name == "" :
        return

    newIP = deepcopy(config['packageIP_configs'][num-1])
    newIP['name'] = name
    config['packageIP_configs'].append(newIP)
    Config.write()
    packagingMenu( newIP )

# De;ete an existing packaging menu to a new name
def deletePackagingMenu():

    # list existing IPs and let the user choose one
    ix = 1
    ch = []
    for ipconfig in config['packageIP_configs']:
        print( str(ix) + ") " + ipconfig['name'] )
        ix+=1
    num = int(raw_input( "Enter the configuration to delete: " ))

    delip = config['packageIP_configs'][num-1]
    pname = delip['name']
    
    really_delete = yesNo( "Are you sure you want to permanently delete the project " + pname + " ?", 'n')

    if not really_delete:
        return

    # delete
    del config['packageIP_configs'][num-1]
    Config.write()

    return

# Menu for existing project
def packagingMenu( packIPConfig ):
    ipPackMenu = ConsoleMenu( "IP Packaging menu" )
    changeParams = FunctionItem( "Change parameters", IPParameterChanger, args=[packIPConfig] )
    ipPackMenu.append_item( changeParams )
    sourceEditingMenuItems( packIPConfig['sources'],packIPConfig['constraints'],packIPConfig['projectIPs'],ipPackMenu)
    maketcl = FunctionItem( "Generate tcl packaging script", makeIPPackagingScript, args=[packIPConfig] )
    ipPackMenu.append_item( maketcl )
    ipPackMenu.start()
    ipPackMenu.join()
    
    
submenu_packIP = ConsoleMenu( "IP packaging menu", prologue_text="Tools to package an IP. Files are copied from the git repositories to a dedicated build area and tcl scripts to package the IP are generated.",
                                formatter=menu_format )
submenu_packIP_item = SubmenuItem( "IP packaging menu", submenu = submenu_packIP )
submenu_packIP_item.set_menu( menu )
menu.append_item( submenu_packIP_item )

for ipconfig in config['packageIP_configs']:
    ipprojmenu = FunctionItem( ipconfig['name'], packagingMenu, args=[ipconfig] )
    submenu_packIP.append_item( ipprojmenu )

ipprojmenu = FunctionItem( "Add a new IP packaging project", newPackagingMenu )
submenu_packIP.append_item( ipprojmenu )
ipcpprojmenu = FunctionItem( "Doublicate an existing IP packaging project", copyPackagingMenu )
submenu_packIP.append_item( ipcpprojmenu )
ipdpprojmenu = FunctionItem( "Delete an existing IP packaging project", deletePackagingMenu )
submenu_packIP.append_item( ipdpprojmenu )


#######################################################################
# Create a utilities menu
#######################################################################

submenu_tools = ConsoleMenu("File listing", "List files in various locations.",
                            formatter=menu_format)
submenu_tools_item = SubmenuItem( "List files", submenu=submenu_tools )
submenu_tools_item.set_menu( menu )

def listFiles(loc, ext):
    print("File List for extension " + ext + " : \n")
    fileitems = findFiles( loc, ext )
    for item in fileitems:
        print( item['path'] )
    raw_input()


itxdc = FunctionItem("List all gitrep .xdc files", listFiles, args=[config['gitRepoPath'], ".xdc"])
itvhd = FunctionItem("List all gitrep .vhd files", listFiles, args=[config['gitRepoPath'], ".vhd"])
itv   = FunctionItem("List all gitrep .v files",   listFiles, args=[config['gitRepoPath'], ".v"])
itxci = FunctionItem("List all gitrep .xci files", listFiles, args=[config['gitRepoPath'], ".xci"])
itdcp = FunctionItem("List all gitrep .dcp files", listFiles, args=[config['gitRepoPath'], ".dcp"])

#ipxdc = FunctionItem("List all iprun .xdc files", listFiles, args=[ip_run, ".xdc"])
#ipvhd = FunctionItem("List all iprun .vhd files", listFiles, args=[ip_run, ".vhd"])
#ipv   = FunctionItem("List all iprun .v files",   listFiles, args=[ip_run, ".v"])
#ipxci = FunctionItem("List all iprun .xci files", listFiles, args=[ip_run, ".xci"])
#ipdcp = FunctionItem("List all iprun .dcp files", listFiles, args=[ip_run, ".dcp"])


submenu_tools.append_item(itxdc)
submenu_tools.append_item(itvhd)
submenu_tools.append_item(itv)
submenu_tools.append_item(itxci)
submenu_tools.append_item(itdcp)

#submenu_tools.append_item(ipxdc)
#submenu_tools.append_item(ipvhd)
#submenu_tools.append_item(ipv)
#submenu_tools.append_item(ipxci)
#submenu_tools.append_item(ipdcp)

#menu_format.show_item_top_border(ipxdc.text, True)
#menu_format.show_item_bottom_border(ipdcp.text, True)

menu.append_item( submenu_tools_item )


menu.start()
menu.join()
